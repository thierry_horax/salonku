import { Component, OnInit } from '@angular/core';
import { SalonService } from '../salon.service';
import { Router } from '@angular/router';
import { ModalController, NavParams, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-review',
  templateUrl: './review.page.html',
  styleUrls: ['./review.page.scss'],
})
export class ReviewPage implements OnInit {
  idlastinserted;
  idsalon;
  login_info;
  list_jasa_yang_direkomendasi = [];
  list_jasa_salon_tertentu;
  list_fasilitas_yang_direkomendasi = [];
  list_fasilitas_salon_tertentu;
  constructor(private navparam:NavParams,private ss:SalonService, private route:Router, private modalcontroller:ModalController, private toastcontroller:ToastController) { }

  ngOnInit() {
    this.login_info = JSON.parse(localStorage.getItem('login_information'));
    this.idsalon = this.navparam.get('idsalon');
    this.idlastinserted = this.navparam.get('idlastinserted');
    this.mengambilFasilitasSalonTertentu();
    this.mengambilJasaSalonTertentu();
  }

  mengambilJasaSalonTertentu()
  {
    this.ss.mengambilSeluruJasaYangAdaPadaSalon(this.idsalon).subscribe((data)=>{
      if(data !=null)
      {
        console.log(data);
        this.list_jasa_salon_tertentu = data;
      }
    });
  }

  mengambilFasilitasSalonTertentu()
  {
    this.ss.mengambilSeluruFasilitasYangAdaPadaSalon(this.idsalon).subscribe((data)=>{
      if(data != null)
      {
        console.log(data);
        this.list_fasilitas_salon_tertentu = data;
      }
    })
  }

  rekomendasi_jasa(event:any)
  {
    console.log(event.target.checked);
    if(event.target.checked == false)
    {
      this.list_jasa_yang_direkomendasi.push(event.target.value);
    }
    else
    {
      for(let i=0; i<this.list_jasa_yang_direkomendasi.length; i++)
      {
        if(event.target.value == this.list_jasa_yang_direkomendasi[i])
        {
          this.list_jasa_yang_direkomendasi.splice(i, 1);
        }
      }
    }
    console.log(this.list_jasa_yang_direkomendasi);
  }

  rekomendasi_fasilitas(event:any)
  {
    if(event.target.checked == false)
    {
      this.list_fasilitas_yang_direkomendasi.push(event.target.value);
    }
    else
    {
      for(let i=0; i<this.list_fasilitas_yang_direkomendasi.length; i++)
      {
        if(event.target.value == this.list_fasilitas_yang_direkomendasi[i])
        {
          this.list_fasilitas_yang_direkomendasi.splice(i, 1);
        }
      }
    }
    console.log(this.list_fasilitas_yang_direkomendasi);
  }

  simpanReview()
  {
    this.ss.menyimpanReview(JSON.stringify(this.list_jasa_yang_direkomendasi),JSON.stringify(this.list_fasilitas_yang_direkomendasi),localStorage.getItem('login_information'),this.idsalon,this.idlastinserted).subscribe((data)=>{
      console.log(data);
      if(data !="")
      {
        this.success();
        this.closeModal();
      }
    })
  }

  closeModal()
  {
    this.modalcontroller.dismiss();
  }

  async success() {
    const toast = await this.toastcontroller.create({
      message: 'Review berhasil disimpan',
      duration: 4000
    });
    toast.present();
  }

}
