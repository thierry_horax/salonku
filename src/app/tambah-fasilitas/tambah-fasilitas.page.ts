import { Component, OnInit } from '@angular/core';
import { FasilitasService } from '../fasilitas.service';
import { ToastController } from '@ionic/angular';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-tambah-fasilitas',
  templateUrl: './tambah-fasilitas.page.html',
  styleUrls: ['./tambah-fasilitas.page.scss'],
})
export class TambahFasilitasPage implements OnInit {
  master_fasilitas;
  id_fasilitas = "";
  constructor(private fs: FasilitasService, private toastController: ToastController, private app: AppComponent) { app.in_etalase_salon = true; }

  ngOnInit() {
    this.mengambilMasterFasilitas();
  }


  mengambilMasterFasilitas() {
    this.fs.mengambilMasterFasilitas().subscribe((data) => {
      if (data != null) {
        this.master_fasilitas = data;
      }
    });
  }

  tambahFasilitas() {
    if (this.id_fasilitas == "") {
      this.fieldKosong();
    }
    else {
      this.fs.tambahFasilitas(this.id_fasilitas, localStorage.getItem('login_information')).subscribe((data) => {
        if (data == 'failed') {
          this.failed();
        }
        else if (data == "success") {
          this.success();
          this.id_fasilitas = "";
        }
      });
    }
  }

  async success() {
    const toast = await this.toastController.create({
      message: 'Fasilitas berhasil ditambahkan',
      duration: 4000
    });
    toast.present();
  }

  async failed() {
    const toast = await this.toastController.create({
      message: 'Fasilitas sudah ada',
      duration: 4000
    });
    toast.present();
  }

  async fieldKosong() {
    const toast = await this.toastController.create({
      message: 'Silahkan pilih salah satu fasilitas',
      duration: 4000
    });
    toast.present();
  }
}
