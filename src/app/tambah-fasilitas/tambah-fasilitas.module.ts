import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TambahFasilitasPage } from './tambah-fasilitas.page';

const routes: Routes = [
  {
    path: '',
    component: TambahFasilitasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TambahFasilitasPage]
})
export class TambahFasilitasPageModule {}
