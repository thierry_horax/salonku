import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TambahFasilitasPage } from './tambah-fasilitas.page';

describe('TambahFasilitasPage', () => {
  let component: TambahFasilitasPage;
  let fixture: ComponentFixture<TambahFasilitasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahFasilitasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TambahFasilitasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
