import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  username:string = "";
  password:string ="";
  constructor(public ls:LoginService,private router:Router,private toastController:ToastController) { 
     
  }

  ngOnInit() {
    if(localStorage.getItem('login_information')!=null)
    {
      this.router.navigateByUrl('/home');
    }
  }

  getUsername(event:any)
  {
    this.username = event.target.value;
  }

  getPassword(event:any)
  {
    this.password = event.target.value;
  }

  login()
  {
    if(this.username.length ==0)
    {
      this.alertFieldUsernameKosong();
    } 
    else if(this.password.length==0)
    {
      this.alertFieldPasswordKosong();
    }
    else
    {
      this.ls.login(this.username,this.password).subscribe((data)=>{
        if(data!='gagal')
        {
          localStorage.setItem('login_information', JSON.stringify(data));
          console.log(localStorage.getItem('login_information'));
          this.router.navigateByUrl('/home');
        }
        else
        {
          this.alertUsernameAtauPasswordSalah();
        }
      });
    }
  }

  /*async alertAkunBelumTerdaftar() {
    const toast = await this.toastController.create({
      message: 'Akun belum terdaftar',
      duration: 4000
    });
    toast.present();
  }
*/
  async alertUsernameAtauPasswordSalah() {
    const toast = await this.toastController.create({
      message: 'Username atau password salah',
      duration: 4000
    });
    toast.present();
  }

  async alertFieldUsernameKosong() {
    const toast = await this.toastController.create({
      message: 'Username harus diisi',
      duration: 3000
    });
    toast.present();
  }

  async alertFieldPasswordKosong() {
    const toast = await this.toastController.create({
      message: 'Password harus diisi',
      duration: 4000
    });
    toast.present();
  }
  
}
