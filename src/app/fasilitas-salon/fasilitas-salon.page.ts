import { Component, OnInit } from '@angular/core';
import { FasilitasService } from '../fasilitas.service';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { SearchService } from '../search.service';
import { LoginService } from '../login.service';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-fasilitas-salon',
  templateUrl: './fasilitas-salon.page.html',
  styleUrls: ['./fasilitas-salon.page.scss'],
})
export class FasilitasSalonPage implements OnInit {
  list_fasilitas;
  constructor(private toastController:ToastController,private ac:AlertController,private fs:FasilitasService, private route:Router, private app:AppComponent, private loadingScreen:LoadingController) { app.in_etalase_salon=true; }

  ngOnInit() {
    this.menampilkanSeluruhFasilitasSalon();
  }

  menampilkanSeluruhFasilitasSalon()
  {
    this.fs.mengambilSeluruhFasilitasSalon(localStorage.getItem('login_information')).subscribe((data)=>{
      if(data!=null)
      {
        this.list_fasilitas = data;
      }
    });
  }

  menujuHalamanTambahFasilitas()
  {
    this.route.navigateByUrl('/tambah-fasilitas');
  }

  menghaapusFasilitasDariList(argIdFasilitas)
  {
    this.fs.menghapusFasilitas(argIdFasilitas, localStorage.getItem('login_information')).subscribe((data)=>{
      this.success();
      this.menampilkanSeluruhFasilitasSalon();
    });
  }

  async refresh(event:any)
  {
    const loading = await this.loadingScreen.create({
      message : "Mohon tunggu..."
    });
    loading.present();
    setTimeout(() => {
      this.menampilkanSeluruhFasilitasSalon();
      event.target.complete();
      loading.dismiss();
    }, 2000);
  }

  async success() {
    const toast = await this.toastController.create({
      message: 'Fasilitas berhasil dihapus',
      duration: 4000
    });
    toast.present();
  }

  async removeFasilitas(argIdFasilitas)
  {
    const alert = await this.ac.create({
      header:"Pemberitahuan",
      message:"Apakah anda yakin ingin menghapus fasilitas ini?",
      buttons: [{
        text:'Cancel',
        role:'cancel',
        handler:()=>{

        }
      },
      {
        text:'Ok',
        handler:async () =>{
          this.menghaapusFasilitasDariList(argIdFasilitas);
        }
      }
      ]
    });
    await alert.present();
  }
}
