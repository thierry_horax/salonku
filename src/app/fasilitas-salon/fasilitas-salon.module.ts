import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FasilitasSalonPage } from './fasilitas-salon.page';

const routes: Routes = [
  {
    path: '',
    component: FasilitasSalonPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FasilitasSalonPage]
})
export class FasilitasSalonPageModule {}
