import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { SalonService } from '../salon.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NavParams } from '@ionic/angular';
import { ConfigURLService } from '../config-url.service';

@Component({
  selector: 'app-informasi-jasa',
  templateUrl: './informasi-jasa.page.html',
  styleUrls: ['./informasi-jasa.page.scss'],
})
export class InformasiJasaPage implements OnInit {
  url = "";
  idsalon = "";
  idjasa = "";
  nama_jasa = "";
  harga = "";
  keterangan = "";
  list_gambar = [];

  sliderOptions = {
    centeredSlides: true,
  }

  constructor(private cu: ConfigURLService, private app: AppComponent, private salon: SalonService, private route: Router, private ar: ActivatedRoute) { app.in_etalase_salon = true; }

  ngOnInit() {
    this.url = this.cu.URL_SERVER_FOR_IMAGES;
    //UNTUK MENGAMBIL VALUE PARAMETER DARI URL
    this.ar.params.subscribe(params => {
      this.idsalon = params['idsalon'];
      this.idjasa = params['idjasa'];
    });

    this.salon.mengambilInformasiJasaTertentu(this.idsalon, this.idjasa).subscribe((data) => {
      if (data != null) {
        this.nama_jasa = data['nama_jasa'];
        this.harga = data['harga'];
        this.keterangan = data['keterangan'];
      }
    });

    this.mengambilGambarJasaDariServer();
  }

  mengambilGambarJasaDariServer() {
    this.salon.mengambilGambarJasaPadaSalonTertentu(this.idjasa, this.idsalon).subscribe((data: any) => {
      if (data != null) {
        for (let gambar of data) {
          this.list_gambar.push(gambar);
        }
      }
    })
  }

  menujuHalamanEditJasaTertentu() {
    this.route.navigate(['/edit-jasa', this.idsalon, this.idjasa]);
  }

}
