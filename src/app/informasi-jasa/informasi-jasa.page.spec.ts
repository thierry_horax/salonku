import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformasiJasaPage } from './informasi-jasa.page';

describe('InformasiJasaPage', () => {
  let component: InformasiJasaPage;
  let fixture: ComponentFixture<InformasiJasaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformasiJasaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformasiJasaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
