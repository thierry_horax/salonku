import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InformasiJasaPage } from './informasi-jasa.page';

const routes: Routes = [
  {
    path: '',
    component: InformasiJasaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InformasiJasaPage]
})
export class InformasiJasaPageModule {}
