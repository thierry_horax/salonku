import { Component, OnInit } from '@angular/core';
import { PengaturanService } from '../pengaturan.service';
import { ToastController } from '@ionic/angular';
import { UserService } from '../user.service';

@Component({
  selector: 'app-pengaturan',
  templateUrl: './pengaturan.page.html',
  styleUrls: ['./pengaturan.page.scss'],
})
export class PengaturanPage implements OnInit {
  master_subkriteria;
  master_kriteria;
  selected_kriteria: string[] = [];
  selected_fasilitas: string[] = [];
  kriteria: string[] = [];
  subkriteria: string[] = [];
  kriteria_user = "";

  constructor(private config: UserService, private toastController: ToastController) { }

  ngOnInit() {
    this.getMasterKriteria();
    this.getKriteriaUser();
    this.getSubKriteriaUser();
  }

  save() {
    if (this.displayContainerReorderFasilitas() == true && this.selected_fasilitas.length == 0) {
      this.pilihsubkriteria();
    }
    else if (this.displayContainerReorderFasilitas() == false && this.selected_kriteria.length == 0) {
      this.pilihkriteria();
    }
    else {
      this.config.saveKriteriaAndSubkriteria(localStorage.getItem('login_information'), JSON.stringify(this.selected_kriteria), JSON.stringify(this.selected_fasilitas)).subscribe((data) => {
        console.log(data);
        if (data == "success") {
          this.success();
          this.getKriteriaUser();
          this.getSubKriteriaUser();
        }
      });
    }
  }

  getKriteriaUser() {
    this.config.getKriteriaUser(localStorage.getItem('login_information')).subscribe((data) => {
      this.kriteria = data;
      this.selected_kriteria = data;
    });
  }

  getSubKriteriaUser() {
    this.config.getSubkriteriaUser(localStorage.getItem('login_information')).subscribe((data) => {
      console.log(data);
      this.subkriteria = data;
      this.selected_fasilitas = data;
    });
  }

  getMasterSubkriteria(argidkriteria) {
    this.config.getSubkriteria(argidkriteria).subscribe((data) => {
      if (data != null) {
        this.master_subkriteria = data;
      }
    });
  }

  getMasterKriteria() {
    this.config.getMasterKriteria().subscribe((data) => {
      this.master_kriteria = data;
    });
  }

  reorderSelectedKriteria(event) {
    //mengambil kriteria yang dipindahkan
    let itemMoved = this.selected_kriteria[event.detail['from']];

    //menghapus kriteria yang dipindahkan
    this.selected_kriteria.splice(event.detail['from'], 1);

    //memasukkan kembali kriteria yang dipindahkan
    this.selected_kriteria.splice(event.detail['to'], 0, itemMoved);

    //menyimpan array berdasarkan urutan kriteria
    this.selected_kriteria = this.selected_kriteria;
    event.detail.complete();
  }

  selectedKriteria(argMasterKriteria) {
    let selectedkriteria = null;
    for (let selected_kriteria of this.kriteria) {
      if (selected_kriteria['idKriteria'] == argMasterKriteria['idKriteria']) {
        selectedkriteria = selected_kriteria;
        break;
      }
    }

    if (selectedkriteria == null) {
      return argMasterKriteria;
    }
    else {
      return selectedkriteria;
    }
  }

  selectedSubkriteria(argMasterSubkriteria) {
    let selectedSubkriteria = null;
    for (let selected_subkriteria of this.subkriteria) {
      if (selected_subkriteria['idsubkriteria'] == argMasterSubkriteria['idsubkriteria']) {
        selectedSubkriteria = selected_subkriteria;
        break;
      }
    }

    if (selectedSubkriteria == null) {
      return argMasterSubkriteria;
    }
    else {
      return selectedSubkriteria;
    }
  }

  isFasilitasContainInSelectedKriteria(kriteria): boolean {
    //mengecek apakah didalam array selected_kriteria terdapat object kriteria yang mimiliki value property "nama_kriteria" adalah fasilitas
    return kriteria.nama_kriteria == "Fasilitas";
  }
  displayContainerReorderFasilitas() {
    //menampilkan container reorder fasilitas
    return this.selected_kriteria.some(this.isFasilitasContainInSelectedKriteria);
  }
  reorderSelectedFasilitas(event) {
    //mengambil kriteria yang dipindahkan
    let itemMoved = this.selected_fasilitas[event.detail['from']];

    //menghapus kriteria yang dipindahkan
    this.selected_fasilitas.splice(event.detail['from'], 1);

    //memasukkan kembali kriteria yang dipindahkan
    this.selected_fasilitas.splice(event.detail['to'], 0, itemMoved);

    //menyimpan array berdasarkan urutan kriteria
    this.selected_fasilitas = this.selected_fasilitas;
    event.detail.complete();
  }

  refreshSelectedKriteria() {
    this.selected_kriteria = this.kriteria;

    //jika ada fasilitas dalam kriteria yang dipilih, maka akan mengambil master fasilitas dari database
    if (this.displayContainerReorderFasilitas() == true) {
      this.getMasterSubkriteria(3);
    }
    else {
      this.selected_fasilitas = [];
      this.master_subkriteria = "";
    }
  }

  refreshSelectedFasilitas() {
    this.selected_fasilitas = this.subkriteria;
  }

  async success() {
    const toast = await this.toastController.create({
      message: 'Pengaturan berhasil disimpan',
      duration: 4000
    });
    toast.present();
  }

  async pilihkriteria() {
    const toast = await this.toastController.create({
      message: 'Silahkan pilih kriteria',
      duration: 2000
    });
    toast.present();
  }

  async pilihsubkriteria() {
    const toast = await this.toastController.create({
      message: 'Silahkan pilih subkriteria',
      duration: 2000
    });
    toast.present();
  }
}
