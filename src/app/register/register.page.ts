import { Component, OnInit, NgZone } from '@angular/core';
import { LoginService } from '../login.service';
import { ToastController, Events } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  nama: string = "";
  username: string = "";
  password: string = "";
  kriteria: string[] = [];
  fasilitas: string[] = [];
  selected_kriteria: string[] = [];
  selected_fasilitas: string[] = [];
  master_kriteria = "";
  master_fasilitas = "";

  constructor(private ls: LoginService, private toastController: ToastController, private router: Router) {

  }

  ngOnInit() {
    this.getMasterKriteria();
  }

  reorderSelectedKriteria(event) {
    //mengambil kriteria yang dipindahkan
    let itemMoved = this.selected_kriteria[event.detail['from']];

    //menghapus kriteria yang dipindahkan
    this.selected_kriteria.splice(event.detail['from'], 1);

    //memasukkan kembali kriteria yang dipindahkan
    this.selected_kriteria.splice(event.detail['to'], 0, itemMoved);

    //menyimpan array berdasarkan urutan kriteria
    this.selected_kriteria = this.selected_kriteria;
    event.detail.complete();
  }

  reorderSelectedFasilitas(event) {
    //mengambil kriteria yang dipindahkan
    let itemMoved = this.selected_fasilitas[event.detail['from']];

    //menghapus kriteria yang dipindahkan
    this.selected_fasilitas.splice(event.detail['from'], 1);

    //memasukkan kembali kriteria yang dipindahkan
    this.selected_fasilitas.splice(event.detail['to'], 0, itemMoved);

    //menyimpan array berdasarkan urutan kriteria
    this.selected_fasilitas = this.selected_fasilitas;
    event.detail.complete();
  }


  getNama(event: any) {
    this.nama = event.target.value;
  }

  getUsername(event: any) {
    this.username = event.target.value;
  }

  getPassword(event: any) {
    this.password = event.target.value;
  }

  getMasterKriteria() {
    this.ls.getMasterKriteria().subscribe((data) => {
      this.master_kriteria = data;
    });
  }

  getSubkriteria(argidkriteria) {
    this.ls.getSubkriteria(argidkriteria).subscribe((data) => {
      console.log(data);
      this.master_fasilitas = data;
    });
  }


  isFasilitasContainInSelectedKriteria(kriteria): boolean {
    //mengecek apakah didalam array selected_kriteria terdapat object kriteria yang mimiliki value property "nama_kriteria" adalah fasilitas
    return kriteria.nama_kriteria == "Fasilitas";
  }

  displayContainerReorderFasilitas() {
    //menampilkan container reorder fasilitas
    return this.selected_kriteria.some(this.isFasilitasContainInSelectedKriteria);
  }

  refreshSelectedKriteria() {
    this.selected_kriteria = this.kriteria;

    //jika ada fasilitas dalam kriteria yang dipilih, maka akan mengambil master fasilitas dari database
    if (this.displayContainerReorderFasilitas() == true) {
      this.getSubkriteria(3);
    }
    else {
      this.selected_fasilitas = [];
      this.master_fasilitas = "";
    }
  }

  refreshSelectedFasilitas() {
    this.selected_fasilitas = this.fasilitas;
  }

  register() {
    if (this.username.length == 0 || this.password.length == 0 || this.nama.length == 0) {
      this.alertFieldKosong();
    }
    else if (this.displayContainerReorderFasilitas() == true && this.selected_fasilitas.length == 0) {
      this.pilihsubkriteria();
    }
    else if (this.displayContainerReorderFasilitas() == false && this.selected_kriteria.length == 0) {
      this.pilihkriteria();
    }
    else {
      this.ls.register(this.nama, this.username, this.password, JSON.stringify(this.selected_kriteria), JSON.stringify(this.selected_fasilitas)).subscribe((data) => {
        if (data != "gagal") {
          localStorage.setItem('login_information', JSON.stringify(data));
          this.router.navigateByUrl('/home');
        }
      });
    }

  }

  async alertFieldKosong() {
    const toast = await this.toastController.create({
      message: 'Field masih ada yang kosong',
      duration: 3000
    });
    toast.present();
  }
  async pilihkriteria() {
    const toast = await this.toastController.create({
      message: 'Silahkan pilih kriteria',
      duration: 2000
    });
    toast.present();
  }

  async pilihsubkriteria() {
    const toast = await this.toastController.create({
      message: 'Silahkan pilih subkriteria',
      duration: 2000
    });
    toast.present();
  }
}
