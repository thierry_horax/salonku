import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { SalonService } from '../salon.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ConfigURLService } from '../config-url.service';

@Component({
  selector: 'app-edit-jasa',
  templateUrl: './edit-jasa.page.html',
  styleUrls: ['./edit-jasa.page.scss'],
})
export class EditJasaPage implements OnInit {
  url = "";
  selected_files: File[] = [];
  string: String[] = [];
  idsalon = "";
  idjasa = "";
  harga = "";
  keterangan = "";
  nama_jasa = "";
  list_gambar = [];

  sliderOptions = {
    centeredSlides: true,
    slidesPerView: 1.6
  }

  constructor(private cu: ConfigURLService, private app: AppComponent, private salon: SalonService, private route: Router, private ar: ActivatedRoute, private toastController: ToastController) { app.in_etalase_salon = true; }

  ngOnInit() {
    this.url = this.cu.URL_SERVER_FOR_IMAGES;
    this.ar.params.subscribe(params => {
      this.idsalon = params['idsalon'];
      this.idjasa = params['idjasa'];
    });

    this.salon.mengambilInformasiJasaTertentu(this.idsalon, this.idjasa).subscribe((data) => {
      if (data != null) {
        this.nama_jasa = data['nama_jasa'];
        this.harga = data['harga'];
        this.keterangan = data['keterangan'];
      }
    });

    this.mengambilGambarJasaDariServer();
  }

  mengambilGambarJasaDariServer() {
    this.salon.mengambilGambarJasaPadaSalonTertentu(this.idjasa, this.idsalon).subscribe((data: any) => {
      if (data != null) {
        for (let gambar of data) {
          this.list_gambar.push(gambar);
        }
      }
    })
  }

  getListImage(event: any) {
    this.selected_files = event.target.files;
    console.log(this.selected_files.length);
  }

  simpanPerubahanJasa() {
    if (this.harga != null) {
      const fd = new FormData();
      fd.append('idsalon', this.idsalon);
      fd.append('idjasa', this.idjasa);
      fd.append('jenis', 'gambarjasa');
      for (let image of this.selected_files) {
        fd.append('images[]', image, image.name);
      }


      this.salon.mengubahInformasiJasaTertentu(this.idjasa, this.idsalon, this.harga, this.keterangan).subscribe((data) => {
        if (data == "success") {
          if (this.selected_files.length > 0) {
            this.salon.uploadGambar(fd).subscribe((data) => {
              if (data == "success") {
                this.success();
              }
            });
          }
          else {
            this.success();
          }
        }
      });
    }
    else {
      this.fieldKosong();
    }
  }

  async fieldKosong() {
    const toast = await this.toastController.create({
      message: 'Harga belum diisi',
      duration: 4000
    });
    toast.present();
  }

  async success() {
    const toast = await this.toastController.create({
      message: 'Perubahan informasi berhasil disimpan',
      duration: 4000
    });
    toast.present();
  }
}
