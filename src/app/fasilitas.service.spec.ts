import { TestBed } from '@angular/core/testing';

import { FasilitasService } from './fasilitas.service';

describe('FasilitasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FasilitasService = TestBed.get(FasilitasService);
    expect(service).toBeTruthy();
  });
});
