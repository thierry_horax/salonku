import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TambahJasaPage } from './tambah-jasa.page';

describe('TambahJasaPage', () => {
  let component: TambahJasaPage;
  let fixture: ComponentFixture<TambahJasaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahJasaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TambahJasaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
