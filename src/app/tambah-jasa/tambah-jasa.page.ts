import { Component, OnInit } from '@angular/core';
import { SalonService } from '../salon.service';
import { SearchService } from '../search.service';
import { ToastController } from '@ionic/angular';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-tambah-jasa',
  templateUrl: './tambah-jasa.page.html',
  styleUrls: ['./tambah-jasa.page.scss'],
})
export class TambahJasaPage implements OnInit {
  id_jasa = "";
  harga = "";
  keterangan="";
  master_jasa;
  constructor(private salon:SalonService, private sv:SearchService, private toastController:ToastController, private app:AppComponent) { app.in_etalase_salon=true; }

  ngOnInit() {
    this.mengambilSemuaJasa();
  }

  tambahJasa()
  {
    if(this.id_jasa != "" && this.harga !="")
    {
      this.salon.tambahJasa(this.id_jasa,this.harga,this.keterangan,localStorage.getItem("login_information")).subscribe((data)=>{
        console.log(data);
        if(data=='success')
        {
          this.id_jasa = "";
          this.harga="";
          this.keterangan="";
          this.success();
        }
        else if(data=="failed")
        {
          this.failed();
        }
      });
    }
    else if(this.id_jasa == "" || this.harga =="")
    {
      this.fieldKosong();
    }
  }

  mengambilSemuaJasa()
  {
    this.sv.getJasaSalon().subscribe((data)=>{
      this.master_jasa= data;
    });
  }

  async success() {
    const toast = await this.toastController.create({
      message: 'Jasa berhasil ditambahkan',
      duration: 4000
    });
    toast.present();
  }

  async failed() {
    const toast = await this.toastController.create({
      message: 'Jasa sudah ada',
      duration: 4000
    });
    toast.present();
  }

  async fieldKosong() {
    const toast = await this.toastController.create({
      message: 'Jasa belum dipilih atau harga belum diisi',
      duration: 4000
    });
    toast.present();
  }
}
