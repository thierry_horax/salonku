import { TestBed } from '@angular/core/testing';

import { ConfigURLService } from './config-url.service';

describe('ConfigURLService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigURLService = TestBed.get(ConfigURLService);
    expect(service).toBeTruthy();
  });
});
