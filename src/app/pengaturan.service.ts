import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigURLService } from './config-url.service';

@Injectable({
  providedIn: 'root'
})
export class PengaturanService {

  constructor(private hc:HttpClient,private cu:ConfigURLService) { }

  getKriteriaUser(argLoginInformation):Observable<any>{
    let param = new HttpParams();
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getkriteriauser.php",param);
  }

  getSubkriteriaUser(argLoginInformation):Observable<any>{
    let param = new HttpParams();
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getsubkriteriauser.php",param);
  }

  getMasterKriteria():Observable<string>{
    let param = new HttpParams();
    return this.hc.post<string>(this.cu.URL_SERVER+"/php_ta/getKriteria.php", param);
  }

  getSubkriteria(argIdkriteria):Observable<string>{
    let param = new HttpParams();
    param = param.set('idkriteria',argIdkriteria);
    return this.hc.post<string>(this.cu.URL_SERVER+"/php_ta/getsubkriteria.php", param);
  }

  saveKriteriaAndSubkriteria(argLoginInformation,argKriteria:string,argSubKriteriaFasilitas:string)
  {
    let param = new HttpParams();
    param = param.set('login_information',argLoginInformation);
    param = param.set('kriteria',argKriteria);
    param = param.set('subkriteriafasilitas',argSubKriteriaFasilitas)
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/savepengaturanuser.php",param);
  }
}
