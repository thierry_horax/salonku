import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { ConfigURLService } from './config-url.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private cu:ConfigURLService, private hc:HttpClient) { }

  getKriteriaUser(argLoginInformation):Observable<any>{
    let param = new HttpParams();
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getkriteriauser.php",param);
  }

  getSubkriteriaUser(argLoginInformation):Observable<any>{
    let param = new HttpParams();
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getsubkriteriauser.php",param);
  }

  getMasterKriteria():Observable<string>{
    let param = new HttpParams();
    return this.hc.post<string>(this.cu.URL_SERVER+"/php_ta/getKriteria.php", param);
  }

  getSubkriteria(argIdkriteria):Observable<string>{
    let param = new HttpParams();
    param = param.set('idkriteria',argIdkriteria);
    return this.hc.post<string>(this.cu.URL_SERVER+"/php_ta/getsubkriteria.php", param);
  }

  saveKriteriaAndSubkriteria(argLoginInformation,argKriteria:string,argSubKriteriaFasilitas:string)
  {
    let param = new HttpParams();
    param = param.set('login_information',argLoginInformation);
    param = param.set('kriteria',argKriteria);
    param = param.set('subkriteriafasilitas',argSubKriteriaFasilitas)
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/savepengaturanuser.php",param);
  }

  getSalonYangDituju(argLoginInformation)
  {
    let param = new HttpParams();
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getsalonyangdituju.php",param);
  }
}
