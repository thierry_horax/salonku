import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ConfigURLService } from './config-url.service';
import { MapService } from './map.service';
import { DatePipe, DecimalPipe } from '@angular/common';
import { InfoSalonModalPage } from './info-salon-modal/info-salon-modal.page';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { ReviewPage } from './review/review.page';
import { ReminderPage } from './reminder/reminder.page';
import { HomePage } from './home/home.page';
@NgModule({
  declarations: [AppComponent, InfoSalonModalPage, ReviewPage],
  entryComponents: [InfoSalonModalPage, ReviewPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ConfigURLService,
    MapService,
    LocalNotifications,
    DatePipe,
    DecimalPipe,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
