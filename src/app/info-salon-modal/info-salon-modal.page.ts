import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { SalonService } from '../salon.service';
import { ConfigURLService } from '../config-url.service';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-info-salon-modal',
  templateUrl: './info-salon-modal.page.html',
  styleUrls: ['./info-salon-modal.page.scss'],
})
export class InfoSalonModalPage implements OnInit {
  url;
  salon;
  list_gambar = [];
  list_gambar_jasa = [];
  list_jasa_yang_dipilih;
  list_informasi_jasa = [];
  list_fasilitas = [];
  sliderOptions = {
    centeredSlides: true,
  }

  constructor(private modalcontroller: ModalController, private navparam: NavParams, private ss: SalonService, private cu: ConfigURLService, private decimalpipe: DecimalPipe) { }

  ngOnInit() {
    this.url = this.cu.URL_SERVER_FOR_IMAGES;
    this.salon = JSON.parse(this.navparam.get('salon'));
    this.list_jasa_yang_dipilih = JSON.parse(this.navparam.get('jasa_salon'));

    this.mengambilInformasiJasaYangDipilih();
    this.mengambilGambarSalon();
    this.mengambilGambarJasa();
    this.mengambilFasilitasSalon();
  }

  limitDecimal(argNumber) {
    return this.decimalpipe.transform(argNumber, '1.2-2');
  }

  closeModal() {
    this.modalcontroller.dismiss()
  }

  mengambilFasilitasSalon() {
    this.ss.mengambilSeluruFasilitasYangAdaPadaSalon(this.salon['idSalon']).subscribe((data: any) => {
      if (data != null) {
        this.list_fasilitas = data;
      }
    })
  }

  mengambilGambarSalon() {
    this.ss.mengambilGambarSalonTertentu(this.salon['idSalon']).subscribe((data: any) => {
      if (data != null) {
        this.list_gambar = data;
        console.log(this.list_gambar);

      }
    })
  }

  mengambilGambarJasa() {
    for (let jasa of this.list_jasa_yang_dipilih) {
      this.ss.mengambilGambarJasaPadaSalonTertentu(jasa, this.salon['idSalon']).subscribe((data: any) => {
        if (data != null) {
          for (let gambar of data) {
            this.list_gambar_jasa.push(gambar);
          }
        }
      })
    }
  }

  mengambilInformasiJasaYangDipilih() {
    for (let idjasa of this.list_jasa_yang_dipilih) {
      this.ss.mengambilInformasiJasaTertentu(this.salon['idSalon'], idjasa).subscribe((data) => {
        if (data != null) {
          this.list_informasi_jasa.push(data);

        }
      });
    }
  }
}
