import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InfoSalonModalPage } from './info-salon-modal.page';

const routes: Routes = [
  {
    path: '',
    component: InfoSalonModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InfoSalonModalPage]
})
export class InfoSalonModalPageModule {}
