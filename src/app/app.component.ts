import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public in_etalase_salon = false;
  public appPagesNotHasSalon = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Riwayat',
      url: '/riwayat',
      icon: 'list'
    },
    {
      title: 'Reminder',
      url: '/reminder',
      icon: 'clock'
    },
    {
      title: 'Register Salon',
      url: '/register-salon',
      icon: 'contact'
    }
    ,
    {
      title: 'Pengaturan',
      url: '/pengaturan',
      icon: 'settings'
    },
    {
      title: 'Log out',
      url: '/logout',
      icon: 'log-out'
    }
  ];

  public appPagesHasSalon = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Riwayat',
      url: '/riwayat',
      icon: 'list'
    },
    {
      title: 'Reminder',
      url: '/reminder',
      icon: 'clock'
    },
    {
      title: 'Open Etalase Salon',
      url: '/home-salon',
      icon: 'business'
    }
    ,
    {
      title: 'Pengaturan',
      url: '/pengaturan',
      icon: 'settings'
    },
    {
      title: 'Log out',
      url: '/logout',
      icon: 'log-out'
    }
  ];

  public appPagesInEtalaseSalon = [
    {
      title: 'Home',
      url: '/home-salon',
      icon: 'home'
    },
    {
      title: 'Fasilitas',
      url: '/fasilitas-salon',
      icon: 'list'
    },
    {
      title: 'Pengaturan',
      url: '/pengaturan-salon',
      icon: 'settings'
    },
    {
      title: 'Keluar dari etalase',
      url: '/home',
      icon: 'log-out'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.hasSalon();
    });
  }

  hasSalon(): boolean {
    if (localStorage.getItem('login_information') != null) {
      let login_information = JSON.parse(localStorage.getItem('login_information'));
      if (login_information['idSalon'] != null && this.in_etalase_salon == false) {
        return true;
      }
      else if (login_information['idSalon'] == null && this.in_etalase_salon == false) {
        return false;
      }
    }

  }
}
