import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule' 
  },
  {
    path: 'list',
    loadChildren:'./list/list.module#ListPageModule'
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'logout', loadChildren: './logout/logout.module#LogoutPageModule' },
  { path: 'pengaturan', loadChildren: './pengaturan/pengaturan.module#PengaturanPageModule' },
  { path: 'register-salon', loadChildren: './register-salon/register-salon.module#RegisterSalonPageModule' },
  { path: 'home-salon', loadChildren: './home-salon/home-salon.module#HomeSalonPageModule' },
  { path: 'tambah-jasa', loadChildren: './tambah-jasa/tambah-jasa.module#TambahJasaPageModule' },
  { path: 'fasilitas-salon', loadChildren: './fasilitas-salon/fasilitas-salon.module#FasilitasSalonPageModule' },
  { path: 'tambah-fasilitas', loadChildren: './tambah-fasilitas/tambah-fasilitas.module#TambahFasilitasPageModule' },
  { path: 'edit-fasilitas', loadChildren: './edit-fasilitas/edit-fasilitas.module#EditFasilitasPageModule' },
  { path: 'pengaturan-salon', loadChildren: './pengaturan-salon/pengaturan-salon.module#PengaturanSalonPageModule' },
  { path: 'informasi-jasa/:idsalon/:idjasa', loadChildren: './informasi-jasa/informasi-jasa.module#InformasiJasaPageModule' },
  { path: 'edit-jasa/:idsalon/:idjasa', loadChildren: './edit-jasa/edit-jasa.module#EditJasaPageModule' },
  { path: 'info-salon-modal', loadChildren: './info-salon-modal/info-salon-modal.module#InfoSalonModalPageModule' },
  { path: 'review', loadChildren: './review/review.module#ReviewPageModule' },
  { path: 'riwayat', loadChildren: './riwayat/riwayat.module#RiwayatPageModule' },  { path: 'reminder', loadChildren: './reminder/reminder.module#ReminderPageModule' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
