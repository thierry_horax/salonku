import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchService } from '../search.service';
import * as mapboxgl from 'mapbox-gl';
import { MapService } from '../map.service';
import { LoginService } from '../login.service';
import { Router, ActivatedRoute } from '@angular/router';
import MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions'
import '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions.css'
import { AppComponent } from '../app.component';
import { async } from '@angular/core/testing';
import { ModalController, ToastController, Platform, LoadingController, AlertController, IonSlides } from '@ionic/angular';
import { InfoSalonModalPage } from '../info-salon-modal/info-salon-modal.page';
import { ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';
import { Plugins } from '@capacitor/core';
import { ReviewPage } from '../review/review.page';
import { DecimalPipe, Location } from '@angular/common';
import * as moment from 'moment';
import { ReminderPage } from '../reminder/reminder.page';
import { equal } from 'assert';
const { LocalNotifications } = Plugins;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  durasi_reminder = 0;
  jasa: string[] = [];
  master_jasa: string = "";
  map: mapboxgl.Map;
  popup: mapboxgl.Popup;
  marker: mapboxgl.Marker;
  geolocate: mapboxgl.GeolocateControl;
  direction: MapboxDirections;
  array_marker: mapboxgl.Marker = [];
  displayTimer = false;
  result = [];
  final = [];
  temp_final = [];
  dest_point = [];
  onDirection = false;
  previousIdx = 0;
  @ViewChild('testSlide', { static: false }) slides: IonSlides;

  sliderOptions = {
    centeredSlides: true,
    slidesPerView: 1.3
  }


  constructor(private sv: SearchService, private ms: MapService, private ls: LoginService, private router: Router, private app: AppComponent, private modalcontroller: ModalController, private alertctrl: AlertController, private loadingScreen: LoadingController, private toastController: ToastController, private decimalpipe: DecimalPipe, private route: ActivatedRoute) {
    app.in_etalase_salon = false;
  }

  ngOnInit() {
    this.final = [];
    this.temp_final = [];
    this.isLogin();
    this.menampilkanMap();
    this.mengambilLokasiUserSaatIni();
    this.mengambilLongitutdeLatitudePosisiUser();
    this.mengambilSemuaJasa();
    this.menginisialisasiDirection();

    this.route.queryParams.subscribe(params => {
      if (Object.keys(params).length != 0) {
        this.final.push(JSON.parse(params['object_salon']));

      }
    })
  }

  async getActivedIndex() {
    /*if (this.array_marker[this.previousIdx].getPopup().isOpen() == true) {
      this.array_marker[this.previousIdx].togglePopup();
    }*/
    await this.slides.getActiveIndex().then(idx => {
      if (this.previousIdx < idx) {
        this.array_marker[idx - 1].togglePopup();
      }
      else {
        this.array_marker[idx + 1].togglePopup();
      }
      this.previousIdx = idx;
      let salon = this.final[idx];
      this.map.flyTo({ center: [salon['longitude'], salon['latitude']] });
      this.array_marker[idx].togglePopup();

    })
  }

  limitDecimal(argNumber) {
    return this.decimalpipe.transform(argNumber, '1.2-2');
  }

  roundingDecimal(argNumber) {
    return this.decimalpipe.transform(argNumber, '1.0-0');
  }

  showTimer() {
    if (this.displayTimer == true) {
      this.displayTimer = false;
    }
    else {
      this.displayTimer = true;
    }
  }

  isLogin() {
    if (localStorage.getItem('login_information') == null) {
      this.router.navigateByUrl('');
    }
  }

  reminderNotification(argDurasi, argObjectSalon) {
    let time = new Date(argDurasi["Tanggal"] + "T" + argDurasi["Waktu"]);
    LocalNotifications.schedule({
      notifications: [{
        title: "Pemberitahuan",
        body: "Silahkan menuju ke salon tersebut",
        id: 1,
        schedule: { at: time },
        sound: "assets/sounds/alarm.wav",
        attachments: null,
        actionTypeId: "",
        extra: null,
      }]
    });
    this.final = [];
    this.clearAllMarker();
  }

  menampilkanMap() {
    //menginisialisasi mapbox sebagai map
    Object.getOwnPropertyDescriptor(mapboxgl, "accessToken").set(this.ms.token);
    this.map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      zoom: 8,

      center: [112.7661496, -7.3191244]
    });
    this.map.addControl(new mapboxgl.NavigationControl());
  }

  membuatPopUp(argNamaSalon, argX, argY) {

    return new mapboxgl.Popup({ offset: 15 }).setText(argNamaSalon);
  }

  async membuatMarker(argX, argY, argPopup) {
    this.marker = new mapboxgl.Marker({
      draggable: false
    });
    this.marker.setLngLat([argX, argY]).setPopup(argPopup).addTo(this.map)
    this.array_marker.push(this.marker);
  }

  mengambilLokasiUserSaatIni() {

    //menginisialisasi geolocation dengan map
    let geolocate = new mapboxgl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true
      },
      trackUserLocation: true
    });
    this.map.addControl(geolocate);

    //membuat otomatis mengambil lokasi pengguna saat ini
    this.map.on('load', function () {
      geolocate.trigger();

    });
    this.geolocate = geolocate;
  }

  mengambilLongitutdeLatitudePosisiUser() {
    this.geolocate.on('geolocate', function (event: any) {
      this.long = event.coords.longitude;
      this.lat = event.coords.latitude;
      localStorage.setItem('pointUser', JSON.stringify([this.long, this.lat]));
    });
  }

  mengambilSemuaJasa() {
    this.sv.getJasaSalon().subscribe((data) => {
      this.master_jasa = data;
    });
  }

  menginisialisasiDirection() {
    this.direction = new MapboxDirections({
      accessToken: this.ms.token,
      unit: 'metric',
      profile: "mapbox/driving",
      alternatives: true,
      zoom: 12,
      controls: {
        inputs: false,
        instructions: false,
      }
    });
  }

  async menampilkanInformasiSalon(argSalon) {
    const modal = await this.modalcontroller.create({
      component: InfoSalonModalPage,
      componentProps: { salon: JSON.stringify(argSalon), jasa_salon: JSON.stringify(this.jasa) },
      backdropDismiss: true
    })

    return await modal.present();
  }

  async menampilkanFormReview(argSalon, argLastIdInserted) {
    const modal = await this.modalcontroller.create({
      component: ReviewPage,
      componentProps: { idsalon: argSalon, idlastinserted: argLastIdInserted },
      backdropDismiss: true
    })

    modal.onDidDismiss().then(
      (data) => {
        this.jasa = [];
        this.final = [];
        this.temp_final = [];
        this.clearAllMarker();
        this.geolocate.trigger();
      }
    )

    return await modal.present();
  }


  async mengarahkanKeSalon(argX, argY, argObjectSalon) {
    this.map.addControl(this.direction);
    this.temp_final = this.final;
    if (this.array_marker.length > 0) {
      this.clearAllMarker();
    }
    this.final = [];
    this.final.push(argObjectSalon);
    let point = JSON.parse(localStorage.getItem('pointUser'));
    this.direction.setOrigin([point[0], point[1]]);
    this.direction.setDestination([argX, argY]);
    this.onDirection = true;
    this.membuatMarker(argObjectSalon['longitude'], argObjectSalon['latitude'], this.membuatPopUp(argObjectSalon['nama_salon'], argObjectSalon['longitude'], argObjectSalon['latitude']));
    this.alertPerjalananAndaAkanDimulai();
  }

  cancelDirection() {
    this.clearAllMarker();
    this.final = this.temp_final;
    this.onDirection = false;
    this.map.removeControl(this.direction);

    for (let salon of this.final) {
      let long = parseFloat(salon['longitude']);
      let lat = parseFloat(salon['latitude']);
      this.membuatMarker(long, lat, this.membuatPopUp(salon['nama_salon'], salon['longitude'], salon['latitude']));
    }

    this.geolocate.trigger();
    this.alertPerjalananDibatalkan();
  }

  clearAllMarker() {
    for (let marker of this.array_marker) {
      marker.remove();
    }
    this.array_marker = [];
  }

  selesaikanPerjalanan() {
    this.sv.selesaikanPerjalanan(localStorage.getItem('login_information'), this.final[0]['idSalon'], JSON.stringify(this.jasa)).subscribe((data: any) => {
      if (data != "") {
        this.alertTelahTiba();
        this.menampilkanFormReview(this.final[0]['idSalon'], data);
      }
    })
  }

  async search() {
    if (this.jasa.length > 0) {
      const loading = await this.loadingScreen.create({
        message: "Mohon tunggu..."
      });
      loading.present();

      if (this.array_marker.length > 0) {
        this.clearAllMarker();
      }
      let point = JSON.parse(localStorage.getItem('pointUser'));
      this.direction.setOrigin([point[0], point[1]]);
      this.sv.getSalonBaseOnJasa(JSON.stringify(this.jasa), localStorage.getItem('login_information')).subscribe(async (data) => {
        if (data != null) {
          this.result = data;
          for (let i = 0; i < this.result.length; i++) {
            await this.ms.getDistances(point[0], point[1], this.result[i]['longitude'], this.result[i]['latitude']).then(data => {
              this.result[i]['jarak'] = data['routes'][0]['distance'] / 1000;
              this.result[i]['durasi'] = data['routes'][0]['duration'] / 60;
            });
          }
          this.sv.MAGIQ(JSON.stringify(this.result), localStorage.getItem('login_information')).subscribe(async (data: any) => {
            if (data != null) {
              if (data.length == 0) {
                this.alertPencarianTidakDitemukan();
              }
              else {
                this.final = data;
                for (let salon of data) {
                  let long = parseFloat(salon['longitude']);
                  let lat = parseFloat(salon['latitude']);
                  this.membuatMarker(long, lat, this.membuatPopUp(salon['nama_salon'], salon['longitude'], salon['latitude']));

                }
                this.alertPencarianSelesai();
              }
              loading.dismiss();
              this.array_marker[0].togglePopup();
            }

          });

        }
      });
    }
    else {
      this.clearAllMarker();
      this.final = [];
      this.cancelDirection();

    }
  }

  async alertPerjalananDibatalkan() {
    let alert = await this.toastController.create({
      message: 'Perjalanan anda telah dibatalkan',
      duration: 2000
    });
    await alert.present();
  }

  async alertPerjalananAndaAkanDimulai() {
    let alert = await this.toastController.create({
      message: 'Perjalanan anda akan dimulai',
      duration: 2000
    });
    await alert.present();
  }

  async alertPencarianTidakDitemukan() {
    let alert = await this.alertctrl.create({
      header: 'Pemberitahuan',
      message: 'Hasil tidak ditemukan. Silahkan pilih kriteria atau jasa lain.',
      buttons: ['Ok']
    });
    await alert.present();
  }

  async alertTelahTiba() {
    const toast = await this.toastController.create({
      message: 'Anda telah tiba',
      duration: 1500
    });
    toast.present();
  }

  async alertPencarianSelesai() {
    const toast = await this.toastController.create({
      message: 'Hasil telah ditemukan.',
      duration: 1500
    });
    toast.present();
  }

  async alertReminderTelahDIset(argDurasi) {
    const toast = await this.toastController.create({
      message: 'Reminder telah diset untuk ' + argDurasi + " menit",
      duration: 3000
    });
    toast.present();
  }

  async openOptionTimer(argObjectSalon) {
    let tanggal_saat_ini = moment().format("YYYY-MM-DD");
    let waktu_saat_ini = moment().format("HH:mm");
    console.log(waktu_saat_ini);
    let alert = await this.alertctrl.create({
      header: 'Pilih durasi',
      inputs: [
        {
          id: "Waktu",
          name: "Waktu",
          type: 'time',
          value: waktu_saat_ini
        },
        {
          name: "Tanggal",
          type: 'date',
          value: tanggal_saat_ini,

        }],
      buttons: [
        {
          text: "Cancel",
        },
        {
          text: "Pilih",
          handler: data => {
            let reminder_detail = [{ 'salon': argObjectSalon, 'waktu_diingatkan': data, 'jasa_dicari': this.jasa }];
            localStorage.setItem('reminder_detail', JSON.stringify(reminder_detail));
            console.log(data);
            this.reminderNotification(data, argObjectSalon);
            this.alertReminderTelahDIset(data);
          }
        }]
    });
    await alert.present();
  }
}
