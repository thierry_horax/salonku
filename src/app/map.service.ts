import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  public token ="pk.eyJ1IjoibW9la2ljaSIsImEiOiJjazEzN2tnN28wNmp3M2JxZDJlaGljYzZlIn0.9gLZTwHg7fAGuKyQyMS2nw";
  constructor(private hc:HttpClient) { }

  getDistances(argLongitutdeUser,argLatitudeUser,argLongitudeTarget,argLatitudeTarget)
  {
    return this.hc.get("https://api.mapbox.com/directions/v5/mapbox/driving/"+ argLongitutdeUser+","+argLatitudeUser+";"+argLongitudeTarget+"," + argLatitudeTarget+"?geometries=geojson&steps=true&access_token=" + this.token).toPromise();
  }
}
