import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ModalController } from '@ionic/angular';
import { ReviewPage } from '../review/review.page';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-riwayat',
  templateUrl: './riwayat.page.html',
  styleUrls: ['./riwayat.page.scss'],
})
export class RiwayatPage implements OnInit {
  login_info;
  list_salon_yang_dituju = [];
  constructor(private us:UserService, private modalcontroller:ModalController, private datePipe:DatePipe) { }

  ngOnInit() {
    this.login_info = localStorage.getItem('login_information');
    this.mengambilSalonYangTelahDituju();
  }

  mengambilSalonYangTelahDituju()
  {
    this.us.getSalonYangDituju(this.login_info).subscribe((data:any)=>{
      if(data != null)
      {
        this.list_salon_yang_dituju = data;
      }
    })
  } 

  transformDate(argDate):string{
    let oldDate = new Date(argDate);
    let newDate = "";
    newDate = this.datePipe.transform(oldDate, 'dd-MM-yyyy h:mm:ss a');
    return newDate;
  }

  async menampilkanFormReview(argSalon, argLastIdInserted)
  {
    const modal = await this.modalcontroller.create({
      component:ReviewPage,
      componentProps: {idsalon : argSalon, idlastinserted : argLastIdInserted},
      backdropDismiss : true
    })

    modal.onDidDismiss().then(
      (data)=>{
        this.ngOnInit();
      }
    )

    return await modal.present();
  }

}
