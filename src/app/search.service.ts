import { Injectable } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs';
import { ConfigURLService } from './config-url.service';
import { TouchSequence } from 'selenium-webdriver';
@Injectable({
  providedIn: 'root'
})
export class SearchService {
  result = [];
  constructor(private hc:HttpClient,private cu:ConfigURLService) { }
  
  extractResultListSalon(argSelectedJasa,argLoginInformation){
    let res = [];
    this.getSalonBaseOnJasa(argSelectedJasa,argLoginInformation).subscribe((data)=>{
      for(let row of data)
      {
        res.push([row['longitude'],row['latitude'],row['idSalon']]);
      }
    });
    this.result = res;
  }

  getJasaSalon():Observable<string>
  {
    let param = new HttpParams();
    return this.hc.post<string>(this.cu.URL_SERVER + "/php_ta/getalljasa.php",param);
  }
  
  getSalonBaseOnJasa(argSelectedJasa,argLoginInformation):Observable<any>
  {
    let param = new HttpParams();
    param = param.set('selectedjasa', argSelectedJasa);
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getsalonbaseonjasa.php",param);
  }

  selesaikanPerjalanan(argLoginInformation, argIdSalon, argListJasa)
  {
    let param = new HttpParams();
    param = param.set('idsalon_yang_dituju', argIdSalon);
    param = param.set('login_information',argLoginInformation);
    //param = param.set('jasa_yang_dicari',argListJasa);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/selesaikanperjalanan.php",param);
  }

  MAGIQ(argListSalon,argLoginInformation)
  {
    let param = new HttpParams();
    param = param.set('list_salon',argListSalon);
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/MAGIQ.php",param);
  }
}
