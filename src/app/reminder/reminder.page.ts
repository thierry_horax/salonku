import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AlertController, ModalController, ToastController, NavController } from '@ionic/angular';
import { Plugins, LocalNotificationPendingList, LocalNotificationScheduleResult } from '@capacitor/core';
import { InfoSalonModalPage } from '../info-salon-modal/info-salon-modal.page';
import { HomePage } from '../home/home.page';
import { Router, NavigationExtras } from '@angular/router';
const { LocalNotifications } = Plugins;
@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.page.html',
  styleUrls: ['./reminder.page.scss'],
})
export class ReminderPage implements OnInit {
  salon;
  nama_salon = "-";
  alamat_salon = "-";
  waktu_buka = "-";
  jasa_yang_dicari = [];
  waktu_diingatkan = "-";

  constructor(private alertctrl: AlertController, private modalcontroller: ModalController, private toastController: ToastController, private router: Router, private navctrl: NavController) { }

  ngOnInit() {
    let reminder_detail = JSON.parse(localStorage.getItem('reminder_detail'));
    console.log(reminder_detail)
    if (reminder_detail != null) {
      this.salon = reminder_detail[0]['salon'];
      this.nama_salon = this.salon['nama_salon'];
      this.alamat_salon = this.salon['alamat'];
      this.waktu_buka = this.salon['waktu_buka'] + " - " + this.salon['waktu_tutup'];
      this.waktu_diingatkan = reminder_detail[0]['waktu_diingatkan']['Tanggal'] + " " + reminder_detail[0]['waktu_diingatkan']['Waktu'];
      this.jasa_yang_dicari = reminder_detail[0]['jasa_dicari']
    }
  }

  async menampilkanInformasiSalon() {
    if (this.salon != null) {
      const modal = await this.modalcontroller.create({
        component: InfoSalonModalPage,
        componentProps: { salon: JSON.stringify(this.salon), jasa_salon: JSON.stringify(this.jasa_yang_dicari) },
        backdropDismiss: true
      })

      return await modal.present();
    }
  }

  arahkan() {
    let navextra: NavigationExtras = {
      queryParams: {
        object_salon: JSON.stringify(this.salon)
      }
    }
    this.router.navigate(['home'], navextra);
  }

  async openOptionTimer() {
    let tanggal_saat_ini = moment().format("YYYY-MM-DD");
    let waktu_saat_ini = moment().format("HH:mm");
    console.log(waktu_saat_ini);
    let alert = await this.alertctrl.create({
      header: 'Pilih durasi',
      inputs: [
        {
          id: "Waktu",
          name: "Waktu",
          type: 'time',
          value: waktu_saat_ini
        },
        {
          name: "Tanggal",
          type: 'date',
          value: tanggal_saat_ini,

        }],
      buttons: [
        {
          text: "Cancel",
        },
        {
          text: "Pilih",
          handler: async data => {
            await this.hapusReminder('ubah');
            let reminder_detail = [{ 'salon': this.salon, 'waktu_diingatkan': data, 'jasa_dicari': this.jasa_yang_dicari }];
            localStorage.setItem('reminder_detail', JSON.stringify(reminder_detail));
            console.log(reminder_detail);
            this.reminderNotification(data, this.salon);
            this.alertWaktuReminderTelahDiubah();
          }
        }]
    });
    await alert.present();
  }

  async hapusReminder(argAction) {
    let pendinglist: LocalNotificationScheduleResult = await LocalNotifications.getPending();
    LocalNotifications.cancel(pendinglist);
    localStorage.removeItem('reminder_detail');
    if (argAction != "ubah") {
      this.alertReminderTelahDihapus();
    }
  }

  reminderNotification(argDurasi, argObjectSalon) {
    let time = new Date(argDurasi["Tanggal"] + "T" + argDurasi["Waktu"]);
    LocalNotifications.schedule({
      notifications: [{
        title: "Pemberitahuan",
        body: "Silahkan menuju ke salon tersebut",
        id: 1,
        schedule: { at: time },
        sound: "assets/sounds/alarm.wav",
        attachments: null,
        actionTypeId: "",
        extra: null,
      }]
    });
  }

  async alertWaktuReminderTelahDiubah() {
    const toast = await this.toastController.create({
      message: 'Reminder telah diubah',
      duration: 1500
    });
    toast.present();
  }

  async alertReminderTelahDihapus() {
    const toast = await this.toastController.create({
      message: 'Reminder telah dihapus',
      duration: 1500
    });
    toast.present();
  }
}
