import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigURLService {
  //public URL_SERVER: string = "https://tugasakhirsalonku.000webhostapp.com";
  //public URL_SERVER: string = "http://tugasakhirsalonku.epizy.com"
  public URL_SERVER_FOR_IMAGES: string = "https://tugasakhirsalonku.000webhostapp.com/gambar";
  public URL_SERVER: string = "http://192.168.100.13:80";
  //public URL_SERVER: string = "http://172.20.10.2:80";
  constructor() { }
}
