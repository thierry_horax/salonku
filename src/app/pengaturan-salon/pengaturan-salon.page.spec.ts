import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengaturanSalonPage } from './pengaturan-salon.page';

describe('PengaturanSalonPage', () => {
  let component: PengaturanSalonPage;
  let fixture: ComponentFixture<PengaturanSalonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengaturanSalonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengaturanSalonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
