import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PengaturanSalonPage } from './pengaturan-salon.page';

const routes: Routes = [
  {
    path: '',
    component: PengaturanSalonPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PengaturanSalonPage]
})
export class PengaturanSalonPageModule {}
