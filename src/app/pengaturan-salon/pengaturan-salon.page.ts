import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { SalonService } from '../salon.service';
import { ToastController } from '@ionic/angular';
import * as mapboxgl from 'mapbox-gl';
import { MapService } from '../map.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { ConfigURLService } from '../config-url.service';
@Component({
  selector: 'app-pengaturan-salon',
  templateUrl: './pengaturan-salon.page.html',
  styleUrls: ['./pengaturan-salon.page.scss'],
})
export class PengaturanSalonPage implements OnInit {
  url = "";
  idsalon;
  nama_salon;
  nomor_telepon;
  jam_buka;
  jam_tutup;
  longitude;
  latitude;
  map: mapboxgl.Map;
  marker: mapboxgl.Marker;
  point_marker;
  list_gambar = [];
  selected_files: File[] = [];
  sliderOptions = {
    centeredSlides: true,
  }

  constructor(private cu: ConfigURLService, private app: AppComponent, private salon: SalonService, private toastController: ToastController, private datePipe: DatePipe, private ms: MapService) { app.in_etalase_salon = true; }

  ngOnInit() {
    this.url = this.cu.URL_SERVER_FOR_IMAGES;
    this.salon.mengambilInformasiSalon(localStorage.getItem('login_information')).subscribe((data) => {
      console.log(data);
      if (data != null) {
        this.idsalon = data['idSalon'];
        this.nama_salon = data['nama_salon'];
        this.nomor_telepon = data['nomor_telepon'];
        this.jam_buka = data['waktu_buka'];
        this.jam_tutup = data['waktu_tutup'];
        this.longitude = data['longitude'];
        this.latitude = data['latitude'];
        this.menampilkanMap(this.longitude, this.latitude);
        this.membuatMarker(this.longitude, this.latitude);
        this.point_marker = this.getMarkerLocation();
        this.mengambilGambarSalon(this.idsalon);
      }
    });


  }

  mengambilGambarSalon(argIdSalon) {
    this.salon.mengambilGambarSalonTertentu(argIdSalon).subscribe((data: any) => {
      if (data != null) {
        this.list_gambar = data;
      }
    })
  }

  getListImage(event: any) {
    this.selected_files = event.target.files;
  }

  menampilkanMap(argLongitude, argLatitude) {
    //menginisialisasi mapbox sebagai map
    Object.getOwnPropertyDescriptor(mapboxgl, "accessToken").set(this.ms.token);
    this.map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      zoom: 18,
      center: [argLongitude, argLatitude]
    });
    this.map.addControl(new mapboxgl.NavigationControl());

  }

  membuatMarker(argLongitude, argLatitude) {

    this.marker = new mapboxgl.Marker({
      draggable: true
    }).setLngLat([argLongitude, argLatitude]).addTo(this.map);


  }

  getMarkerLocation() {
    return this.marker.on('dragend', function () { return this._lngLat });
  }

  simpanPerubaha() {
    const fd = new FormData();
    fd.append('idsalon', this.idsalon);
    fd.append('jenis', 'gambarsalon');
    for (let image of this.selected_files) {
      fd.append('images[]', image, image.name);
    }

    if (this.nomor_telepon != "") {
      this.salon.getAddress(this.point_marker._lngLat['lng'], this.point_marker._lngLat['lat']).subscribe((data: any) => {
        if (data != null) {
          this.salon.simpanPerubahanPengaturan(this.nama_salon, this.point_marker._lngLat['lng'], this.point_marker._lngLat['lat'], data.features[0].properties.address, this.nomor_telepon, moment(this.jam_buka, "HH:mm").format("HH:mm"), moment(this.jam_tutup, "HH:mm").format("HH:mm"), localStorage.getItem('login_information')).subscribe((data) => {
            if (data == "success") {
              if (this.selected_files.length > 0) {
                this.salon.uploadGambar(fd).subscribe((data) => {
                  if (data == "success") {
                    this.success();
                  }
                });
              }
              else {
                this.success();
              }
            }
          });
        }
      });
    }
    else {
      this.alertFieldKosong();
    }
  }

  async success() {
    const toast = await this.toastController.create({
      message: 'Pengaturan berhasil disimpan',
      duration: 3000
    });
    toast.present();
  }

  async alertFieldKosong() {
    const toast = await this.toastController.create({
      message: 'Field masih ada yang kosong',
      duration: 3000
    });
    toast.present();
  }
}
