import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { MapService } from '../map.service';
import { SalonService } from '../salon.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { ToastController, IonApp } from '@ionic/angular';
@Component({
  selector: 'app-register-salon',
  templateUrl: './register-salon.page.html',
  styleUrls: ['./register-salon.page.scss'],
})
export class RegisterSalonPage implements OnInit {
  jam_buka;
  jam_tutup;
  nama_salon = "";
  nomor_telepon = "";
  map: mapboxgl.Map;
  marker: mapboxgl.Marker;
  point_marker;
  constructor(private ms: MapService, private salon: SalonService, private datePipe: DatePipe, private router: Router, private toastController: ToastController) { }

  ngOnInit() {
    this.menampilkanMap();
    this.membuatMarker();
    this.point_marker = this.getMarkerLocation();
    console.log(localStorage.getItem('login_information'));
  }

  menampilkanMap() {
    //menginisialisasi mapbox sebagai map
    Object.getOwnPropertyDescriptor(mapboxgl, "accessToken").set(this.ms.token);
    this.map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      zoom: 18,
      center: [112.7661496, -7.3191244]
    });
    this.map.addControl(new mapboxgl.NavigationControl());

  }

  membuatMarker() {

    this.marker = new mapboxgl.Marker({
      draggable: true
    }).setLngLat([112.7661496, -7.3191244]).addTo(this.map);


  }

  getMarkerLocation() {
    return this.marker.on('dragend', function () { return this._lngLat });
  }

  register() {
    if (this.nama_salon.trim() != "" && this.jam_buka != null && this.jam_tutup != null) {
      let jam_buka = new Date(this.jam_buka);
      let jam_tutup = new Date(this.jam_tutup);

      this.salon.getAddress(this.point_marker._lngLat['lng'], this.point_marker._lngLat['lat']).subscribe((data: any) => {
        if (data != null) {
          this.salon.register_salon(this.nama_salon, this.point_marker._lngLat['lng'], this.point_marker._lngLat['lat'], data.features[0].properties.address, this.nomor_telepon, this.datePipe.transform(jam_buka, "HH:mm"), this.datePipe.transform(jam_tutup, "HH:mm"), localStorage.getItem('login_information')).subscribe((data) => {
            console.log(data);
            if (data != "") {
              localStorage.removeItem('login_information');
              localStorage.setItem('login_information', JSON.stringify(data));
              this.router.navigateByUrl('/home-salon');
            }
          });
        }
      });
    }
    else if ((this.jam_buka == null || this.jam_tutup == null) && this.nama_salon.trim() != "") {
      this.alertJamBukaDanJamTutupKosong();
    }
    else if (this.nama_salon.trim() == "" && (this.jam_buka == null || this.jam_tutup == null)) {
      this.alertFieldMasihKosong();
    }
    else if (this.nama_salon.trim() == "") {
      this.alertNamaSalonKosong();
    }
  }

  async alertFieldMasihKosong() {
    const toast = await this.toastController.create({
      message: 'Field nama salon, jam buka dan jam tutup masih kosong',
      duration: 3000
    });
    toast.present();
  }

  async alertNamaSalonKosong() {
    const toast = await this.toastController.create({
      message: 'Silahkan mengisi nama salon',
      duration: 3000
    });
    toast.present();
  }

  async alertJamBukaDanJamTutupKosong() {
    const toast = await this.toastController.create({
      message: 'Silahkan mengisi jam buka dan jam tutup',
      duration: 3000
    });
    toast.present();
  }
}
