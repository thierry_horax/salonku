import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFasilitasPage } from './edit-fasilitas.page';

describe('EditFasilitasPage', () => {
  let component: EditFasilitasPage;
  let fixture: ComponentFixture<EditFasilitasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFasilitasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFasilitasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
