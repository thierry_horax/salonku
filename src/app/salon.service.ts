import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ConfigURLService } from './config-url.service';

@Injectable({
  providedIn: 'root'
})
export class SalonService {

  constructor(private hc:HttpClient, private cu:ConfigURLService) { }

  getAddress(argX,argY)
  {
    return this.hc.get("https://api.mapbox.com/geocoding/v5/mapbox.places/" + argX + "," + argY + ".json?access_token=pk.eyJ1IjoibW9la2ljaSIsImEiOiJjazEzN2tnN28wNmp3M2JxZDJlaGljYzZlIn0.9gLZTwHg7fAGuKyQyMS2nw");
  }

  register_salon(argNamaSalon,argX,argY,argAlamat,argNoTelp,argJamBuka,argJamTutup,argLoginInformation)
  {
    let param = new HttpParams();
    param = param.set('nama_salon',argNamaSalon);
    param = param.set('address',argAlamat);
    param = param.set('no_telp', argNoTelp);
    param = param.set('jam_buka',argJamBuka);
    param = param.set('jam_tutup',argJamTutup);
    param = param.set('longitude',argX);
    param = param.set('latitude',argY);
    param = param.set('login_information', argLoginInformation);

    
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/registersalon.php",param);
  }

  mengambilSeluruJasaYangAdaPadaSalon(argIdSalon)
  {
    let param = new HttpParams();
    param = param.set('idsalon',argIdSalon);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getjasasalontertentu.php",param);
  }

  mengambilSeluruFasilitasYangAdaPadaSalon(argIdSalon)
  {
    let param = new HttpParams();
    param = param.set('idsalon',argIdSalon);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getfasilitassalontertentu.php",param);
  }

  tambahJasa(argIdJasa, argHarga, argKeterangan,argLoginInformation)
  {
    let param = new HttpParams();
    param = param.set('id_jasa', argIdJasa);
    param = param.set('harga', argHarga);
    param = param.set('keterangan',argKeterangan);
    param = param.set('login_information', argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/tambahjasasalontertentu.php",param);
  }

  mengambilInformasiJasaTertentu(argIdSalon, argIdJasa)
  {
    let param = new HttpParams();
    param = param.set('id_jasa',argIdJasa);
    param = param.set('idsalon', argIdSalon);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getinformasijasatertentu.php",param);
  }

  mengubahInformasiJasaTertentu(argIdJasa,argIdSalon, argHarga, argKeterangan)
  {
    let param = new HttpParams();
    param = param.set('id_jasa',argIdJasa);
    param = param.set('idsalon', argIdSalon);
    param = param.set('harga', argHarga);
    param = param.set('keterangan',argKeterangan);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/updateinformasijasatertentu.php",param);
  }

  uploadGambar(argListGambar)
  {
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/uploadgambar.php",argListGambar);
  }

  mengambilInformasiSalon(argLoginInformation)
  {
    let param = new HttpParams();
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getinformasisalon.php",param);
  }

  simpanPerubahanPengaturan(argNamaSalon,argX,argY,argAlamat,argTelepon,argJamBuka,argJamTutup,argLoginInformation)
  {
    let param = new HttpParams();
    param = param.set('nama_salon',argNamaSalon);
    param = param.set('address',argAlamat);
    param = param.set('no_telp', argTelepon);
    param = param.set('jam_buka',argJamBuka);
    param = param.set('jam_tutup',argJamTutup);
    param = param.set('longitude',argX);
    param = param.set('latitude',argY);
    param = param.set('login_information', argLoginInformation);

    
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/savepengaturansalon.php",param);
  }

  mengambilGambarSalonTertentu(argIdSalon)
  {
    let param = new HttpParams();
    param = param.set('idsalon',argIdSalon);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getgambarsalon.php", param);
  }

  mengambilGambarJasaPadaSalonTertentu(argIdJasa,argIdSalon)
  {
    let param = new HttpParams();
    param = param.set('idsalon',argIdSalon);
    param = param.set('idjasa',argIdJasa);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getgambarjasa.php", param);
  }

  menyimpanReview(argListRekomendasiJasa, argListRekomendasiFasilitas,argLoginInformation,argIdSalonYangDiReview, argLastInsertedId)
  {
    let param = new HttpParams();
    param = param.set('listrekomendasijasa', argListRekomendasiJasa);
    param = param.set('listrekomendasifasilitas', argListRekomendasiFasilitas);
    param = param.set('login_information', argLoginInformation);
    param = param.set('salon_yang_direview', argIdSalonYangDiReview);
    param = param.set('idlastinserted', argLastInsertedId);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/review.php",param);
  }
  
}
