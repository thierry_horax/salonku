import { Injectable } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs';
import { ConfigURLService } from './config-url.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public username:string = "";
  public password:string ="";
  constructor(private hc:HttpClient,private cu:ConfigURLService) { }
  
  login(argUsername:string, argPassword:string):Observable<string>{
    let param = new HttpParams();
    param = param.set('username',argUsername);
    param = param.set('password',argPassword);
    return this.hc.post<string>(this.cu.URL_SERVER + '/php_ta/login.php',param);
  }

  register(argNama:string,argUsername:string,argPassword:string,argKriteria:string,argSubKriteriaFasilitas:string):Observable<string>{
    let param = new HttpParams();
    param = param.set('nama',argNama);
    param= param.set('username',argUsername);
    param = param.set('password',argPassword);
    param = param.set('kriteria',argKriteria);
    param = param.set('subkriteriafasilitas',argSubKriteriaFasilitas)
    return this.hc.post<string>(this.cu.URL_SERVER+"/php_ta/register.php",param);
  }

  getMasterKriteria():Observable<string>{
    let param = new HttpParams();
    return this.hc.post<string>(this.cu.URL_SERVER+"/php_ta/getKriteria.php", param);
  }

  getSubkriteria(argIdkriteria):Observable<string>{
    let param = new HttpParams();
    param = param.set('idkriteria',argIdkriteria);
    return this.hc.post<string>(this.cu.URL_SERVER+"/php_ta/getsubkriteria.php", param);
  }
}
