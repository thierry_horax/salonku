import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { SalonService } from '../salon.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home-salon',
  templateUrl: './home-salon.page.html',
  styleUrls: ['./home-salon.page.scss'],
})
export class HomeSalonPage implements OnInit {
  login_info;
  jasa_salon;
  constructor(private app:AppComponent,private salon:SalonService, private route:Router, private loadingScreen:LoadingController) {
    this.app.in_etalase_salon=true;
   }
  
  ngOnInit() {
    this.login_info = JSON.parse(localStorage.getItem('login_information'));
    this.menampilkanJasaSalonTertentu();
    
  }


  async menampilkanJasaSalonTertentu()
  {
    
    this.salon.mengambilSeluruJasaYangAdaPadaSalon(this.login_info['idSalon']).subscribe((data)=>{
      if(data!=null)
      {
        this.jasa_salon = data;
        
      }
    })
  }

  menujuHalamanTambahJasa()
  {
    this.route.navigateByUrl('/tambah-jasa');
  }

  menujuHalamanInformasiJasa(argIdJasa)
  {
    let login_info = JSON.parse(localStorage.getItem('login_information'));
    let idsalon = login_info['idSalon'];
    let idjasa = argIdJasa;
    this.route.navigate(['/informasi-jasa',idsalon, idjasa]); 
  }

  async refresh(event:any)
  {
    const loading = await this.loadingScreen.create({
      message : "Mohon tunggu..."
    });
    loading.present();
    setTimeout(() => {
      this.menampilkanJasaSalonTertentu();
      event.target.complete();
      loading.dismiss();
    }, 2000);
  }
}
