import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSalonPage } from './home-salon.page';

describe('HomeSalonPage', () => {
  let component: HomeSalonPage;
  let fixture: ComponentFixture<HomeSalonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSalonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSalonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
