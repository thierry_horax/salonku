import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ConfigURLService } from './config-url.service';

@Injectable({
  providedIn: 'root'
})
export class FasilitasService {

  constructor(private hc:HttpClient, private cu:ConfigURLService) { }

  mengambilSeluruhFasilitasSalon(argLoginInformation)
  {
    let param = new HttpParams();
    param = param.set('login_information', argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getseluruhfasilitassalon.php",param);
  }
  
  mengambilMasterFasilitas()
  {
    let param = new HttpParams();
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/getallfasilitas.php",param);
  }

  tambahFasilitas(argIdFasilitas,argLoginInformation)
  {
    let param = new HttpParams();
    param = param.set('id_fasilitas',argIdFasilitas);
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/tambahfasilitassalon.php",param);
  }

  menghapusFasilitas(argIdFasilitas, argLoginInformation)
  {
    let param = new HttpParams();
    param = param.set('id_fasilitas',argIdFasilitas);
    param = param.set('login_information',argLoginInformation);
    return this.hc.post(this.cu.URL_SERVER + "/php_ta/hapusfasilitassalon.php",param);
  }
}
